import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import DisableAutocomplete from 'vue-disable-autocomplete';
import router from './router'
import store from './store'


Vue.use(VueResource);
Vue.use(DisableAutocomplete)

//Vue.http.options.credentials = true;
// Vue.http.interceptors.push((request, next) => {
//   request.headers['Authorization'] = 'Bearer osdI47hwBr73CR9LYmSCwJlioYt4Cj6ErmrH7u4dl4PPviyPUg9eQHAUm3myfAZg' //Base64
//   request.headers['Accept'] = 'application/vnd.ibex.v1+json'
//   request.headers['Content-type'] = 'application/json'
//   next()
// })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
