const baseUrl = "https://api.coinmarketcap.com/v2/listings/";
export class apiService {

    getRequest(requestUrl, data) {
        let url = this.baseUrl + requestUrl;
        if (data) {
            this.url = this.url + "?" + data;
        }
        return this.$http.get(this.url).then(
            function (response) {
                return response.body[0];
            },
            function (error) {
                console.log("an error occured in api/local GET");
            }
        );
    }

     postRequest(requestUrl, data) {
         const url = this.baseUrl + requestUrl;
         return this.$http.post(this.url, data, {
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json'
            }
         }).then(
             function (response) {
                 return response.body[0];
             }, function (error) {
                 console.log("an error has occured in api/Post");
             }
         );
    }

};

// Reply of above msg:

// Client ID: 3a6501bb087bb31d
// Client Secret:
// kor0VKCupbeQAS4tCu + jECLvVckxNFmO0N++3mO7IRm88pL9hmdzUXd2EgRt6vsM
// Redirect URI:
// http://node.eprofitanalyzer.com/vuetask

// please follow instructions at http://api-docs.seekom.com/