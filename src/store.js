import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userLogin: [],
    hotels: {}
  },
  mutations: {
    ADD_USER: (state, user) => {
      state.userLogin.push(user)
    },
    ADD_HOTELS: (state, hotel) => {
      state.hotels = {}
      state.hotels = hotel
    }
  },
  actions: {

  },
  getters: {
    userDetails: state => {
      return state.userLogin
    }
  }
})
